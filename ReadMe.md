# Lattice Boltzmann Method

Lattice Boltzmann methods (LBM) is a class of computational fluid dynamics (CFD) methods for fluid simulation

## Cloning the Repository

Using a bash prompt

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
$ mkdir LBM
$ cd LBM
# Pull the source, Note the period in the following line
$ git clone https://gitlab.kitware.com/LBM/lattice-boltzmann-solver.git .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Build Environment

The code provided utilizes C++11 plus CUDA.
- Minimum of CUDA 9.2
- Minimum of GCC 6
- Minimum of MSVC 2017

Our development environments generally consist of combinations of MSVC 2019 with CUDA 10.2 and GCC-6 with CUDA 9.2 and 10.2

Note GCC-5 with CUDA 10.2 failed to build due to C++11 usage in the CUDA file, upgrade to GCC-6 solves this issue.

### Linux Environment

If you are building with VTK support please ensure you have OpenGL and X11 support installed.
To install OpenGL and x11 support :

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
$ sudo apt-get install libgl1-mesa-dev
$ sudo apt-get install libxt-dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Running CMake

The provided project is a CMake project and will require CMake.
Go to the cmake website, <a href="https://cmake.org/download">https://cmake.org/download</a>, and download the appropriate distribution.
Ensure that the CMake bin is on your PATH and available in your cmd/bash shell.

The code is built as part of a CMake 'superbuild', meaning the build is broken into 2 phases, 
  1. A build target will be created that will download any dependent libraries and build them
  2. A build target will be created that will build the code specific to this project, using the newly built libraries.
  
The build is also out of source, meaning the code base is seperated from the build files.
This means you will need two folders, one for the source code and one for the build files.

Run CMake on the source from your build directory.

Once CMake has generated files for the super build, build the superbuild.

If you already have one or more of the dependent libraries built, you may use that build.


### Running CMake GUI

It is suggested to enable Grouped and Advanced in the CMake GUI

The following example shows how to run CMake and use a prebuilt VTK

**Skip step 1 and 3 if you have not prebuilt VTK**

  1 Set USE_SYSTEM_VTK to true
  2 Press `Configure`
  3 A new VTK_DIR variable will be created, set location containing the VTKConfig.cmake file.
    - Generally, this contained in the `<vtk install>/lib/cmake/vtk-8.2` folder.
  4 Press `Generate`

### Running CMake via bash

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
# This assumes you are still in your source directory from the steps above
$ cd ..
$ mkdir LBM_build
$ cd LBM_build
# Generate LBM_build targets
# If you do have VTK prebuilt
$ cmake -DUSE_SYSTEM_VTK=ON -DVTK_DIR=/path/to/VTK/install/lib/cmake/vtk-8.2 ../LBM
# If you do not VTK prebuilt
$ cmake ../LBM
# Build the superbuild project
$ make -j4
# cd into the Innerbuild to make any changes you do to the code
$ cd Innerbuild
$ make
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Building

All executables will be built to the `build/install` directory.

### MSVC Users

  - Open the LatticeBoltzmannMethod.sln in your specified build directory and build one or more configurations (Debug and/or Release)
  - Once built, You may now close this solution file.
  - A new LatticeBoltzmannMethod.sln will be build to your `build/Innerbuild` directory, open it for development.
  - Note if you are using a prebuilt VTK, you will need to add `PATH=/path/to/VTK/install/bin` to your active project Debugging/Environment property

### Bash Users

  - Call make from your build directory
  - Once built, there will be a new makefile your `build/Innerbuild` directory
  - Call make from this Innerbuild directory to build and test any modifications to the code

## Running

A simple driver will be placed in the install/bin directory.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
# *Nix users, you will need to add the lib directory to your library path/to/VTK/install/bin
# You can do this by running a script from your install directory
$ <build/install> source setup_LBM.in
# executable will be placed in your install/bin directory
$ cd <build/dir>/install/bin/
# Run the LBM Legacy Driver
$ ./LBMLegacyDriver
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
