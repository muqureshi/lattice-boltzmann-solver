/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#pragma once
#include <string>
#include <vector>
#include "LBM.h"

///////////////////
// Generic Utils //
///////////////////

LBM_DECL bool CreatePath(std::string const&);

LBM_DECL bool CreateFilePath(std::string const&);

LBM_DECL void DeleteDirectory(std::string const& dir, bool bDeleteSubdirectories = true);

LBM_DECL bool FileExists(std::string const& filename);

LBM_DECL std::string GetCurrentWorkingDirectory();

LBM_DECL void ListFiles(std::string const& dir, std::vector<std::string>& files, std::string const& mask = "");

LBM_DECL void MakeDirectory(std::string const& dir);

LBM_DECL std::string Replace(std::string const& original, std::string const& replace, std::string const& withThis);
