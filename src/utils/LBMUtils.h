/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#pragma once
#include "LBM.h"

namespace LBM
{
  using CSV = std::vector<std::pair<std::string, std::vector<float>>>;

  LBM_DECL bool ReadCSV(std::string const& filename, CSV& data);
  LBM_DECL bool WriteCSV(std::string const& filename, const CSV& data);
  LBM_DECL bool CompareDataSet(LBM::Run const& baseline, LBM::Run const& computed, bool vtr_data_only = false);

  LBM_DECL bool LoadConfiguration(std::string const& config_file, LBM::Run& lbm);
  // Support legacy input decks
  LBM_DECL bool ContainsLegacyScenario(std::string const& dir);
  LBM_DECL bool LoadLegacyScenario(std::string const& legacy_dir, LBM::Run& lbm);

  LBM_DECL double PercentDifference(double expected, double calculated, double epsilon = 1E-20);
  LBM_DECL double PercentTolerance(double expected, double calculated, double epsilon = 1E-20);
}
