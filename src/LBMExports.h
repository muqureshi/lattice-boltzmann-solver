/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#pragma once

#ifdef LBM_EXPORTS
#if defined (__clang__)
#define LBM_DECL
#elif defined(__gnu_linux__)
#define LBM_DECL __attribute__ ((visibility ("default")))
#else
#ifdef LBM_EXPORTS
#define LBM_DECL __declspec(dllexport)
#define LBM_EXP
#else
#define LBM_DECL __declspec(dllimport)
#define LBM_EXP extern
#endif
#endif
#else
#define LBM_DECL
#define LBM_EXP
#endif

#include <map>
#include <string>
#include <vector>
#include <memory>
#include <cstddef>
#include <stdio.h>


#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4251 )
// Explicit template exports.
LBM_EXP template class LBM_DECL std::allocator<int>;
LBM_EXP template class LBM_DECL std::vector<int, std::allocator<int>>;
LBM_EXP template class LBM_DECL std::allocator<float>;
LBM_EXP template class LBM_DECL std::vector<float, std::allocator<float>>;
LBM_EXP template class LBM_DECL std::vector<std::pair<std::string, std::vector<float, std::allocator<float>>>, std::allocator<std::pair<std::string, std::vector<float, std::allocator<float>>>>>;
LBM_EXP template class LBM_DECL std::allocator<char>;
LBM_EXP template struct LBM_DECL std::char_traits<char>;
LBM_EXP template class LBM_DECL std::basic_string<char, std::char_traits<char>, std::allocator<char> >;
LBM_EXP template class LBM_DECL std::allocator<std::basic_string<char, std::char_traits<char>, std::allocator<char> > >;
LBM_EXP template class LBM_DECL std::vector<std::basic_string<char, std::char_traits<char>, std::allocator<char>>, std::allocator<std::basic_string<char, std::char_traits<char>, std::allocator<char>>>>;
LBM_EXP template class LBM_DECL std::allocator <std::pair<const std::string, std::vector<float>>>;
#pragma warning( pop )
#endif
