/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#include "LBM.h"
#include "RunUtils.h"
#ifdef VTK_EXTENSION
  #include "VTKUtils.h"
#endif

int main(int argc, char* argv[])
{
  Logger log("./LBMLegacyDriver.log");
  // Specify which CUDA device to use, or leave black to automatically choose the largest device found.
  if (!cuda_device_check(log))
  {
    log.Error("CUDA device check failed, ensure you have specified a valid CUDA device id.");
    return 1;
  }
  std::stringstream ss;
  LBM::RunConfig runset;

  try
  {
    for (LBM::RunIO run : runset.GetLegacyDirRuns("/cylinder"))
    {
      LBM::Run lbm(run.output_base_path + ".log");
      if (!LBM::LoadLegacyScenario(run.input, lbm))
      {
        lbm.Error("Unable to load configuration file.");
        return 1;
      }
      if (!lbm.Compute())
      {
        lbm.Error("Error running LBM.");
        return 1;
      }
      lbm.Info("Writing outputs to " + run.output_base_path);
      // Metrics
      auto m = LBM::VTK::ComputeMetrics(lbm);
      ss << *m;
      lbm.Info(ss);
#ifdef VTK_EXTENSION
      // Write Things with VTK
      LBM::VTK::WriteMetrics(run.output_base_path, *m);
      LBM::VTK::WriteVTR(run.output_base_path + ".vtr", lbm);
#endif
    }
  }
  catch (std::exception ex)
  {
    log.Error(ex.what());
    return 1;
  }

  return 0;
}
